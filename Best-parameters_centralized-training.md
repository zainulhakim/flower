| Language Group | WER                  | Best WER on Validation Set | Best Hyperparameters                                    |
|----------------|----------------------|----------------------------|---------------------------------------------------------|
| English        | 0.15780998389694043  | 0.14814814814814814        | {'learning_rate': 0.0001, 'per_device_train_batch_size': 4, 'num_train_epochs': 20} |
| Indians        | 0.28502415458937197  | 0.28502415458937197        | {'learning_rate': 0.0001, 'per_device_train_batch_size': 5, 'num_train_epochs': 30} |
| Arabs          | 0.2785829307568438   | 0.2785829307568438         | {'learning_rate': 0.0001, 'per_device_train_batch_size': 5, 'num_train_epochs': 30} |
| Chinese        | 0.22544283413848631  | 0.22383252818035426        | {'learning_rate': 0.0001, 'per_device_train_batch_size': 4, 'num_train_epochs': 30} |
